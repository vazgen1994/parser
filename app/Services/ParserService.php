<?php

namespace App\Services;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Exception;

class ParserService {

 	public function __construct(Client $client)
    {
        $this->url = 'https://vesnaklimat.ru/nastennie-split-sistemi/mit/';
        $this->http = $client;
        $this->headers = [
            'cache-control' => 'no-cache',
            'content-type' => 'application/x-www-form-urlencoded',
        ];
    }

    public function loadCategoryItems(){
    	$results = array();
    	try {
            $response = $this->http->get($this->url); // URL, where you want to 
            $content = $response->getBody()->getContents();
            $crawler = new Crawler( $content );
            $_this = $this;
            $results['brand'] = $crawler->filter('.breadcrumbs ul li')->last()->text();
            $data = $crawler->filter('div.one-product')
                            ->each(function (Crawler $node, $i) use($_this) {
                                return $_this->getItemContent($node);
                            }
                        );
            $results['items'] = $data;
            return $results;
            
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }
    }

    private function getItemContent($node)
    {
        $array = [
            'name' => $this->getName($node),
            'model' => $this->getModel($node),     
            'price' => $this->getPrice($node),          
        ];
        return $array;
    }

    private function hasContent($node)
    {
        return $node->count() > 0 ? true : false;
    }

    private function getName($node){
    	return $this->hasContent($node->filter('.name a')) != false ? $node->filter(' .name a')->text() : '';
    }

    private function getModel($node){
    	$array = explode(" ", $this->hasContent($node->filter('.name a')) != false ? trim($node->filter(' .name a')->text()) : '');
    	return $array[count($array) - 1];
    }

    private function getPrice($node){
    	return $this->hasContent($node->filter('.price span')) != false ? $node->filter('.price span')->text() : '';
    }
}