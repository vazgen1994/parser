<div class="col-lg-4 col-md-4 col-xs-6">
	<div class="card">
		<div class="card-body">
		    <h5 class="card-title">{{ $item['name'] }}</h5>
		    <p><span>Brand : </span> {{ $brand }}</p>
		    <p><span>Model : </span> {{ $item['model'] }}</p>
		    <p><span>Price</span> {{ $item['price'] }}</p>
		  </div>
	</div>
</div>