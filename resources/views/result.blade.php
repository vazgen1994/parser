@extends("layouts.app")

@section("content")
	<div class="container">
		<div class="row">
			@if(isset($results['items']))
				@foreach($results['items'] as $item)
					@include("components.item", ['item' => $item, 'brand' => $results['brand'], 'category' => ''])
				@endforeach
			@endif
		</div>
	</div>
@endsection